package hu.unideb.jmdict.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.KanjiElementInfo;

@Repository
public interface KanjiElementInfoRepository extends JpaRepository<KanjiElementInfo, Long> {

}
