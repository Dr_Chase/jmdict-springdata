package hu.unideb.jmdict.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.Sense;

@Repository
public interface SenseRepository extends JpaRepository<Sense, Long> {

}
