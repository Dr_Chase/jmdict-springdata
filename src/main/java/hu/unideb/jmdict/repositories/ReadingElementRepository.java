package hu.unideb.jmdict.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.ReadingElement;

@Repository
public interface ReadingElementRepository extends JpaRepository<ReadingElement, Long> {

	//@Query(value = "SELECT * FROM readingelement re WHERE re.readingelementtext like %?1%", nativeQuery = true)
	List<ReadingElement> findReadingElementByReadingElementTextContaining(String readingElementText);
}
