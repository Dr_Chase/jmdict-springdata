package hu.unideb.jmdict.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.ReadingElementInformation;

@Repository
public interface ReadingElementInformationRepository extends JpaRepository<ReadingElementInformation, Long> {


}
