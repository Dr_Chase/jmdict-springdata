package hu.unideb.jmdict.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.ReadingElementRestriction;

@Repository
public interface ReadingElementRestrictionRepository extends JpaRepository<ReadingElementRestriction, Long> {

}
