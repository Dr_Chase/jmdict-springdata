package hu.unideb.jmdict.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.KanjiElement;

@Repository
public interface KanjiElementRepository extends JpaRepository<KanjiElement, Long>{

	
	List<KanjiElement> findKanjiElementByKanjiElementContaining(String meaningText);
}
