package hu.unideb.jmdict.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.neovisionaries.i18n.LanguageAlpha3Code;

import hu.unideb.jmdict.jmdict.Gloss;

@Repository
public interface GlossRepository extends JpaRepository<Gloss, Long> {

	//@Query(value = "SELECT * FROM gloss gl WHERE gl.meaningtext like %?1%", nativeQuery = true)
	List<Gloss> findGlossByMeaningTextContaining(String meaningText);
	
	List<Gloss> findGlossByMeaningTextContainingAndLanguageCodeEquals(String meaningText, LanguageAlpha3Code languageCode);
}
