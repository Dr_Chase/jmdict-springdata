package hu.unideb.jmdict.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.ReadingElementPriority;

@Repository
public interface ReadingElementPriorityRepository extends JpaRepository<ReadingElementPriority, Long> {

}
