package hu.unideb.jmdict.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.unideb.jmdict.jmdict.Entry;

@Repository
public interface EntryRepository extends JpaRepository<Entry, Long> {

}
