package hu.unideb.jmdict;

//import org.junit.Test;
//import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;

import com.neovisionaries.i18n.LanguageAlpha3Code;

import hu.unideb.jmdict.jmdict.Entry;
import hu.unideb.jmdict.jmdict.Gloss;
import hu.unideb.jmdict.jmdict.ReadingElement;
import hu.unideb.jmdict.jmdict.Sense;
import hu.unideb.jmdict.repositories.EntryRepository;
import hu.unideb.jmdict.repositories.GlossRepository;
import hu.unideb.jmdict.repositories.ReadingElementRepository;
import hu.unideb.jmdict.repositories.SenseRepository;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class JmDictSpringDataApplicationTests {
	
	@Autowired
	EntryRepository entryRepository;
	
	@Autowired
	GlossRepository glossRepository;
	
	@Autowired
	SenseRepository senseRepository;
	
	@Autowired
	ReadingElementRepository readingElementRepository;

	//@Test
	public void contextLoads() {
	}

	//@Test
	public void firstBasicTest()	{
		
		Entry entry = new Entry();
		
		entry.setEntrySequence(1234L);
		
		entryRepository.save(entry);
		
		ReadingElement re = new ReadingElement(); re.setEntry(entry); re.setReadingElementText("ありがとう");
		readingElementRepository.save(re);
		
		Sense sens = new Sense(); sens.setEntry(entry);
		
		Gloss gloss = new Gloss(); gloss.setSense(sens);
		
		gloss.setLanguageCode(LanguageAlpha3Code.eng);
		gloss.setMeaningText("thank you");
		
		
		senseRepository.save(sens);
		
		glossRepository.save(gloss);
		
	}
}
